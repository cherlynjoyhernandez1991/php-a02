<?php require_once 'code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Loops and Arrays</title>
</head>
<body>
	<h1>Loops and Arrays</h1>
	<h2>Loops</h2>
	<p><?= divisibleByFive(); ?></p>

	<h2>Array Manipulation</h2>

	<h3>Array push</h3>
	<?php array_push($students, 'Cherlyn Joy Hernandez'); ?>
	<pre><?php var_dump($students); ?></pre>

	<h3>Array count</h3>
	<?= count($students); ?>

	<h3>Array push 2</h3>
	<?php array_push($students, 'Naomi Grarcia'); ?>
	<pre><?php var_dump($students); ?></pre>

	<h3>Array count 2</h3>
	<?= count($students); ?>

	<h3>Array remove</h3>
	<?php array_shift($students); ?>
	<pre><?php var_dump($students); ?></pre>

	<h3>Array count 3</h3>
	<?= count($students); ?>
</body>
</html>